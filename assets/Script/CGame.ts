
const {ccclass, property} = cc._decorator;

import Player from './Player';
import Egg from "./Egg";
import ServerSimulator from "./ServerSimulator";
import {PlayerData, ObjectData, EnumType} from "./DataDefine"
import {MSG_CONECTION, MSG_MESSAGE, MSG_ERROR, MSG_CLOSE} from "./DataDefine";

@ccclass
export default class CGame extends cc.Component {

  //score label
  @property({
    type: cc.Label
  })
  labelScore: cc.Label = null;

  @property({
    type: cc.Label
  })
  labelTimer: cc.Label = null;

  @property({
    type: cc.Label
  })
  labelReady: cc.Label = null;

  @property({
    type: cc.Label
  })
  labelHintScore: cc.Label = null;

  @property({
    type: cc.Prefab
  })
  eggPrefab: cc.Prefab = null;
  
  @property({
    type: cc.Prefab
  })
  playerPrefab: cc.Prefab = null;
  
  gameTime: number = 100;

  @property
  numPlayers: number = 5;

  @property({
    type: cc.Node
  })
  btnPlay:cc.Node = null;
  
  
  playerMe:cc.Node = null;
  playerMeData:PlayerData;
  listPlayers : Map<number, cc.Node>;
  serverConnection: ServerSimulator;
  gameState:EnumType;
  timeShowGo;
  listEggs : Map<number, cc.Node>;
  eggPool = null;
  
  // LIFE-CYCLE CALLBACKS:

  onLoad () {
    //create a connection
    this.serverConnection = cc.find("Canvas/ServerSimulator").getComponent(ServerSimulator);
    this.eggPool = new cc.NodePool('Egg');
    
    this.listPlayers = new Map();
    this.listEggs = new Map();
  }

  start()  {
    this.labelScore.enabled = false;
    this.labelTimer.enabled = false;
    this.labelReady.enabled = false;
  }

  onStartGame () {
    this.btnPlay.active = false;
    this.labelScore.enabled = true;
    this.labelTimer.enabled = true;
    this.labelReady.enabled = true;
    this.labelHintScore.enabled = false;
    this.timeShowGo = 1.5;
    this.gameState = EnumType.STATE_INIT;

    //local player joins game
    let myplayer = new PlayerData(-1, EnumType.TYPE_REAL_PLAYER);
    this.Send(MSG_CONECTION, myplayer);

    //create fake n-1 remote players join game
    let i=0;
    for(i=1; i<this.numPlayers; i++)
    {
      var bot = new PlayerData(-1, EnumType.TYPE_BOT_PLAYER);
      this.Send(MSG_CONECTION, bot);
    }
  }


  update (dt) {
    if(this.gameState == EnumType.STATE_INGAME)
    {
      this.labelTimer.string = "Time: "+ Math.round(this.gameTime);
      if(this.timeShowGo > 0)
      {
        this.labelReady.string = "GO!"
        this.timeShowGo -= dt;
        let opa = 255*this.timeShowGo;
        if(opa > 255) opa = 255;
        if(opa < 0) opa = 0;
        this.labelReady.node.opacity = opa;
      }
    }
  }

  public getGameState()
  {
    return this.gameState;
  }
  public getListEggs()
  {
    return this.listEggs;
  }

  addPlayerJoinGame(data:PlayerData)
  {
    if(data.type == EnumType.TYPE_REAL_PLAYER)
    {      
      this.playerMe = cc.instantiate(this.playerPrefab);
      this.node.addChild(this.playerMe);      
      this.listPlayers.set(data.id, this.playerMe);
      //pass init data
      this.playerMe.getComponent('Player').setAutoMove(false);
      this.playerMe.getComponent('Player').setPlayerData(data);
      this.gameState = EnumType.STATE_READY;
    }
    else if(data.type == EnumType.TYPE_BOT_PLAYER)
    {
      var botplayer = cc.instantiate(this.playerPrefab);
      this.node.addChild(botplayer);      
      this.listPlayers.set(data.id, botplayer);

      // pass init data
      botplayer.getComponent('Player').setPlayerData(data);
      //remote players color
      botplayer.getComponent('Player').node.color = new cc.Color(255, 100, 0, 255);
      botplayer.getComponent('Player').node.opacity = 64;
    }
  }

  
  //==========================
  // Message handle
  //==========================
  public onMessage(msg_id, data)
  {
    //receive msg from server
    if(msg_id == MSG_CONECTION)
    {
      if(data.type == EnumType.TYPE_REAL_PLAYER)
      {          
        this.playerMeData = data;
        //create me on the game
        this.addPlayerJoinGame(data);
      }
      else if(data.type == EnumType.TYPE_BOT_PLAYER)
      {
        this.addPlayerJoinGame(data);
      }
      else if(data.type == EnumType.TYPE_NONE)
      {
        //game full, can't join
      }

    }
    else if (msg_id == MSG_MESSAGE)
    {
      let msg = JSON.parse(data);
      if(msg.key == "update")
      {
        this.gameTime = msg.gameTime;
        this.gameState = msg.gameState;

        //sync update player data
        let numPlayer = msg.numPlayer;
        let playerJson = JSON.parse(msg.clientData);
        let i=0;
        for(i=0; i<numPlayer; i++)
        {
          let pdata = playerJson[i];
          let player = this.listPlayers.get(pdata.id);
          if(player)
          {
            player.getComponent("Player").setPlayerData(pdata);
            if(this.playerMeData.id == pdata.id)
            {
              //update my player data and my score
              this.playerMeData = pdata;
              this.labelScore.string = "Score: " + this.playerMeData.score;
            }
          }
        }

        //sync update map data
        let numEgg = msg.numEgg;
        let eggJson = JSON.parse(msg.eggData);
        let newEggMap : Map<number, EnumType> = new Map();
        for(i=0; i<numEgg; i++)
        {
          let edata = eggJson[i];
          newEggMap.set(edata.id, edata.status);
          //update this egg
          let egg = this.listEggs.get(edata.id);
          if(egg)
          {
            //alredy in client game
            //check if it was picked
            if(edata.status == EnumType.STATUS_DISABLE)
            {
              //then remove from client game
              this.eggPool.put(egg);
              this.listEggs.delete(edata.id);
            }
          }
          else
          {
            //the new egg was create from server
            var newEgg = null;
            if(this.eggPool.size() > 0)
            {
              //console.log("create egg from pool!");
              newEgg = this.eggPool.get();
            }
            else
            {
              //create new node
              newEgg = cc.instantiate(this.eggPrefab);
            }
            newEgg.getComponent('Egg').setEggData(edata);
            newEgg.width = edata.rpick;
            newEgg.height = edata.rpick;
            this.listEggs.set(edata.id, newEgg);
            this.node.addChild(newEgg);
          }
        }

        //also check eggs was picked in list then remove
        let keys = Array.from(this.listEggs.keys());
        for(i=0; i<keys.length; i++)
        {
          if(!newEggMap.has(keys[i]))
          {
            let egg = this.listEggs.get(keys[i]);
            this.eggPool.put(egg);
            this.listEggs.delete(keys[i]);
          }
        }
      } //end msg_key "update"
      else if(msg.key == "gameover")
      {
        console.log("Game over !")
        this.gameTime = 0;
        this.gameState = msg.gameState;

        //sync for the last time player data
        let numPlayer = msg.numPlayer;
        let playerJson = JSON.parse(msg.clientData);
        let i=0;
        for(i=0; i<numPlayer; i++)
        {
          let pdata = playerJson[i];
          let player = this.listPlayers.get(pdata.id);
          if(player)
          {
            player.getComponent("Player").setPlayerData(pdata);
            if(this.playerMeData.id == pdata.id)
            {
              //update my player data and my score
              this.playerMeData = pdata;
              this.labelScore.string = "Score: " + this.playerMeData.score;
            }
          }
        }
        this.labelReady.string = "Game Over!"
        this.labelReady.node.opacity = 255;
        this.labelHintScore.string = "Your score: " + this.playerMeData.score;
        this.labelHintScore.enabled = true;
        //@TODO Show the winner
      }
    }
    else if (msg_id == MSG_CLOSE)
    {
      //game end
    }    
  }

  //send msg to server from player
  public Send(msg_id, data) {     
    //console.log(" Cgame call Send !!!");
    //socket.send(message);
    this.serverConnection.onMessage(msg_id, data);
  }
}
