import {EnumType, PlayerData, MSG_MESSAGE} from "./DataDefine"
import CGame from "./CGame";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    //Player info
    playerData:PlayerData;
    
    
    //Moving
    isAI: boolean = true;
    aiTime:number = 0;
    eggFind = null;
    actionMoveLeft = false;
    actionMoveRight = false;
    actionMoveUp = false;
    actionMoveDown = false;

    //CGame
    cgame:CGame;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //set input control
        //add keyboard input listener
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
	  }

    start () {
      this.cgame = cc.find("Canvas/GameMgr").getComponent(CGame);
    }

    update (dt) {
        if(this.cgame.getGameState() != EnumType.STATE_INGAME)
        {
          //game not start yet.
          return;
        }
        if(this.isAI)
        {
          this.aiTime -= dt;
          //sort AI for remote player, go to nearest egg
          //let eggFind = this.findNearestEggs();
          if(this.aiTime < 0)
          {
            this.eggFind = this.getRandomEgg();
            this.aiTime = 5 + Math.random()*4;
          }
                      
          this.playerData.action = EnumType.MOVE_NONE;
          if(this.eggFind)
          {            
            
            //console.log("find egg " + this.eggFind)
            let dx = this.node.position.x - this.eggFind.x;
            let dy = this.node.position.y - this.eggFind.y;
            if( dx > 10)
            {
              this.node.x -= this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_LEFT;
            }
            else if( dx < -10)
            {
              this.node.x += this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_RIGHT;
            }
            else if(dy < -10)
            {
              this.node.y += this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_UP;
            }
            else if(dy > 10)
            {
              this.node.y -= this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_DOWN;
            }
          }
        }
        else
        {
            if(this.actionMoveLeft){
              this.node.x -= this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_LEFT;
            }
            else if(this.actionMoveRight)
            {
              this.node.x += this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_RIGHT;
            }
            else if(this.actionMoveUp)
            {
              this.node.y += this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_UP;
            }
            else if(this.actionMoveDown)
            {
              this.node.y -= this.playerData.speed * dt;
              this.playerData.action = EnumType.MOVE_DOWN;
            }
            else{
              this.playerData.action = EnumType.MOVE_NONE;
            }
        }

        // SendInfo to GS
        this.cgame.Send(MSG_MESSAGE, this.playerData);
        
    }

    getRandomEgg()
    {
      var listEgg = this.cgame.getListEggs();
      let keys = Array.from(listEgg.keys());
      let idegg = Math.round(Math.random()*(keys.length-1));
      return listEgg.get(keys[idegg]).position;
    }

    findNearestEggs()
    {
      var nearestEgg = null;
      var distMin = 9999;
      var listEgg = this.cgame.getListEggs();
      let keys = Array.from(listEgg.keys());
      let i=0
      for(i=0; i<keys.length; i++)
      {
        let egg = listEgg.get(keys[i]);
        let eggPos = egg.position;
        var dist = this.node.position.sub(eggPos).mag();
        if(dist < distMin)
        {
          distMin = dist;
          nearestEgg = eggPos;
        }
      }
      return nearestEgg;
    }

    setAutoMove(auto : boolean){
      this.isAI = auto;
    }
    setPlayerData(data : PlayerData){
      //Sync player data
      this.playerData = data;
      this.node.x = data.px;
      this.node.y = data.py;
    }

    onKeyDown(e:cc.Event.EventCustom) {    
        switch(e.keyCode){
            case cc.macro.KEY.right:
              this.actionMoveRight = true;
              break;
            case cc.macro.KEY.left : 
              this.actionMoveLeft = true;
              break;
            case cc.macro.KEY.up : 
              this.actionMoveUp = true;
            break;
            case cc.macro.KEY.down : 
              this.actionMoveDown = true;
            break;
        }
      }
    
      onKeyUp(e:cc.Event.EventCustom){
        switch(e.keyCode){
          case cc.macro.KEY.right:
            this.actionMoveRight = false;
            break;
          case cc.macro.KEY.left : 
            this.actionMoveLeft = false;
            break;
          case cc.macro.KEY.up : 
            this.actionMoveUp = false;
          break;
          case cc.macro.KEY.down : 
            this.actionMoveDown = false;
          break;
      }
    }
}
