import {ObjectData, EnumType} from "./DataDefine"
const {ccclass, property} = cc._decorator;

@ccclass
export default class Egg extends cc.Component {    
    
    eggData : ObjectData;
    onLoad () {

    }


    update (dt) {    
    }

    setEggData(data : ObjectData){
        //Sync player data
        this.eggData = data;
        this.node.x = data.px;
        this.node.y = data.py;
      }
}