
import {PlayerData, ObjectData, EnumType} from "./DataDefine"
import {MSG_CONECTION, MSG_MESSAGE, MSG_ERROR, MSG_CLOSE} from "./DataDefine";
import CGame from "./CGame"

const {ccclass, property} = cc._decorator;

@ccclass
export default class ServerSimulator extends cc.Component {

    @property
    maxPlayers: number = 5;

    @property
    playerMoveSpeed: number = 100;

    @property
    numEggs: number = 10;

    @property
    radiusPickupEgg: number = 50;

    @property
    gameTime: number = 120;

    @property
    gameMapW: number = 960;

    @property
    gameMapH: number = 640;

    cgameConnection: CGame;

    clientListPlayers: Array<PlayerData>;
    eggsList: Array<ObjectData>;
    indexID:number;
    Timer:number;
    gameState : EnumType;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {        
        this.clientListPlayers = new Array();
        this.cgameConnection = cc.find("Canvas/GameMgr").getComponent(CGame);
    }

    generateGameMap()
    {
        //create randomly eggs on map game
        this.eggsList = new Array();
        console.log("[GS]: Generate eggs map!")
        let i = 0;
        for(i = 0; i< this.numEggs; i++)
        {            
            var newegg = new ObjectData(this.idCreator(), EnumType.TYPE_EGG);
            newegg.px = Math.random() * this.gameMapW;
            newegg.py = 50 + Math.random() * (this.gameMapH-50);
            newegg.rpick = this.radiusPickupEgg;
            this.eggsList.push(newegg);
            //console.log(" egg new " + i + " x = " + newegg.px)
        }        
        console.log("[GS]: Number egg: " +  this.eggsList.length)

    }

    start () {
        this.indexID = 0;
        this.Timer = 5;
        //generate game map with eggs 
        this.generateGameMap();
        this.gameState = EnumType.STATE_INIT;
    }

    update (dt) {

        if(this.gameState == EnumType.STATE_INIT)
        {
            if(this.clientListPlayers.length > 1)
            {
                //game ready to go in 5s
                //other players can continue join in.
                this.Timer = 5;
                this.gameState = EnumType.STATE_READY;
                console.log("[GS]: Game ready in 5s !!");
            }
            //else Wait for players join game
        }
        else if(this.gameState == EnumType.STATE_READY)
        {
            this.Timer -= dt;
            this.msgBoardCast("update");
            if(this.Timer < 0) 
            {
                this.gameState = EnumType.STATE_INGAME;
                console.log("[GS]: Game go !!");
            }
        }
        else if(this.gameState == EnumType.STATE_INGAME)
        {
            //update player pos
            this.updatePlayerAction(dt);
            //check pick up and scoring
            this.checkPickup();
            //countdown game time
            this.gameTime -= dt;
            if(this.gameTime < 0)
            {
                this.gameState = EnumType.STATE_ENDGAME;
                this.msgBoardCast("gameover");
                console.log("[GS]: Game over !!");
            }
        }

        if(this.gameState > EnumType.STATE_INIT && this.gameState < EnumType.STATE_ENDGAME)
        {
            //check time for sync with clients in 0.1s - 0.5s
            this.Timer -= dt;
            if(this.Timer < 0)
            {
                this.Timer = 0.1  + Math.random() * 0.4;
                this.msgBoardCast("update");
            }
        }
    }

    updatePlayerAction(dt)
    {
        let i = 0;
        for(i = 0; i< this.clientListPlayers.length; i++)
        {
            if(this.clientListPlayers[i].action == EnumType.MOVE_LEFT)
            {
                this.clientListPlayers[i].px -= this.clientListPlayers[i].speed * dt;
            }
            else if(this.clientListPlayers[i].action == EnumType.MOVE_RIGHT)
            {
                this.clientListPlayers[i].px += this.clientListPlayers[i].speed * dt;
            }
            else if(this.clientListPlayers[i].action == EnumType.MOVE_UP)
            {
                this.clientListPlayers[i].py += this.clientListPlayers[i].speed * dt;
            }
            else if(this.clientListPlayers[i].action == EnumType.MOVE_DOWN)
            {
                this.clientListPlayers[i].py -= this.clientListPlayers[i].speed * dt;
            }            
        }
    }

    checkPickup()
    {
        //check if player pickup egg
        let i = 0;
        for(i = 0; i< this.clientListPlayers.length; i++)
        {
            let j = 0;
            for(j = 0; j<this.eggsList.length; j++)
            {
                if(this.eggsList[j].status == EnumType.STATUS_ENABLE)
                {
                    let distx = this.clientListPlayers[i].px - this.eggsList[j].px;
                    let disty = this.clientListPlayers[i].py - this.eggsList[j].py;
                    let distpick = this.radiusPickupEgg - Math.sqrt(distx*distx + disty*disty);
                    if(distpick > 0) //can pick egg?
                    {
                        this.eggsList[j].status = EnumType.STATUS_DISABLE; // this egg disable, other players can't pick
                        this.clientListPlayers[i].score++;
                    }
                }
                
            }
        }

        //Update add new egg? immediately?
        let j = 0;
        for(j = 0; j<this.eggsList.length; j++)
        {
            if(this.eggsList[j].status == EnumType.STATUS_DISABLE)
            {
                this.eggsList[j].id = this.idCreator(); //new egg, new id
                this.eggsList[j].px = Math.random() * this.gameMapW;
                this.eggsList[j].py = Math.random() * this.gameMapH;
                this.eggsList[j].status = EnumType.STATUS_ENABLE;
            }            
        }        
        //
    }

    msgBoardCast(msg:string)
    {
        //sync to all players
        var data = JSON.stringify({
            "key"  : msg,
            "gameState": this.gameState,
            "gameTime": this.gameTime,
            "numEgg"   : this.eggsList.length,
            "numPlayer" : this.clientListPlayers.length,
            "eggData" : JSON.stringify(this.eggsList),
            "clientData":JSON.stringify(this.clientListPlayers)
        })
        this.cgameConnection.onMessage(MSG_MESSAGE, data);
    }

    public onMessage(msg_id, data)
    {        
        //console.log("[GS]: Server receives msg: " + msg_id);
        if(msg_id == MSG_CONECTION)
        {
            this.onConnection(data)
        }
        else if (msg_id == MSG_MESSAGE)
        {
            let i = 0;
            for(i = 0; i< this.clientListPlayers.length; i++)
            {
                if(this.clientListPlayers[i].id == data.id)
                {
                    this.clientListPlayers[i].action = data.action;
                    break;
                }
            }
        }
    }

    onConnection(data:PlayerData)
    {        
        //if new player can join game
        if(this.clientListPlayers.length < this.maxPlayers)
        {
            var newplayer = new PlayerData(this.idCreator(), data.type);
            //set starting player pos
            newplayer.px = Math.random() * this.gameMapW;
            newplayer.py = 25;
            newplayer.speed = this.playerMoveSpeed;
            this.clientListPlayers.push(newplayer);

            //send back msg
            this.cgameConnection.onMessage(MSG_CONECTION, newplayer);
        }
        else
        {
            //game room full
            data.type = EnumType.TYPE_NONE
            this.cgameConnection.onMessage(MSG_CONECTION, data);
        }        
    }

    idCreator()
    {
        //simple ID for players & eggs
        this.indexID += 1;
        return this.indexID;
    }
}
