
//player & egg data
export class PlayerData { 
    id : number;
    px : number;
    py : number;
    speed: number;
    action: any;
    status: EnumType;
    score: number;
    key : string;
    type : EnumType;
    constructor(oID:number, type:EnumType)
    {
        this.id = oID;
        this.score = 0;
        this.type = type;
        this.px = 0;
        this.py = 0;
        this.status = EnumType.STATUS_ENABLE;
        this.action = EnumType.MOVE_NONE;
    }
}

export class ObjectData { 
    id : number;
    px : number;
    py : number;
    status: EnumType;
    type : EnumType;
    rpick: number;
    constructor(oID:number, type:EnumType)
    {
        this.id = oID;
        this.type = type;
        this.px = 0;
        this.py = 0;
        this.rpick = 0;
        this.status = EnumType.STATUS_ENABLE;
    }
}

//data pack for sending client-gs
export class DataPack {
    key : string;
    data : ObjectData;
}

export enum EnumType{
    STATUS_DISABLE,
    STATUS_ENABLE,

    MOVE_LEFT,
    MOVE_RIGHT,
    MOVE_UP,
    MOVE_DOWN,
    MOVE_NONE,

    TYPE_REAL_PLAYER,
    TYPE_BOT_PLAYER,
    TYPE_EGG,
    TYPE_NONE,

    STATE_INIT,
    STATE_READY,
    STATE_INGAME,
    STATE_ENDGAME,
    STATE_NONE
}

export const MSG_CONECTION = 'connection';
export const MSG_MESSAGE = 'message';
export const MSG_ERROR = 'error';
export const MSG_CLOSE = 'close';
